def calc_fib(x):
   return x if x<=1 else calc_fib(x-1) + calc_fib(x-2)

for i in range(8):
	print(calc_fib(i), end=" ")