def is_palindrome(s):
   return s[::-1].lower() == s.lower()

print(is_palindrome('Bob'))