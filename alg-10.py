def is_prime(x):
	if x==2: return True
	if x < 2 or x%2 == 0: return False
	for i in range(2,x):
		if x%i==0: return False
	return True

for x in range(0,20):
	print('{}={}'.format(x,is_prime(x)))